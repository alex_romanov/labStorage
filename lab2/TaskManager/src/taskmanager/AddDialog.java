package taskmanager;

import org.jdesktop.swingx.JXDatePicker;

import javax.swing.*;
import java.awt.event.*;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

//панельку времени

public class AddDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField textField1;
    private JTextField textField2;
    private JFormattedTextField formattedTextField2;
    private JPanel panel1;
    private JSpinner spinner1;
    private JPanel panel2;
    private Listener listener;
    private JXDatePicker jxDatePicker;
    private SpinnerDateModel spinnerDateModel;
    private Task task;


    public AddDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        formattedTextField2.setValue(new String());


        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    public void setTask(Task task) {
        this.task = task;
        textField1.setText(task.getName());
        spinnerDateModel.setValue(task.getDateTime());
        jxDatePicker.setDate(task.getDateTime());
        textField2.setText(task.getDescription());

        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < task.getContacts().size(); i++){
            stringBuilder.append(task.getContacts().get(i));
            if(i != task.getContacts().size() - 1){
                stringBuilder.append(",");
            }
        }
        formattedTextField2.setValue(stringBuilder);
    }

    public Task getTask() {
        return task;
    }

    private void createUIComponents() { // соеденить со временем
        JXDatePicker picker = new JXDatePicker();
        panel1 = new JPanel();
        jxDatePicker = picker;
        panel1.add(picker);

        panel2 = new JPanel();
        Date date = new Date();
        spinnerDateModel = new SpinnerDateModel(date, null, null, Calendar.HOUR_OF_DAY);
        spinner1 = new JSpinner(spinnerDateModel);
        JSpinner.DateEditor dateEditor = new JSpinner.DateEditor(spinner1, "HH:mm:ss");
        spinner1.setEditor(dateEditor);
        panel2.add(spinner1);
    }

    public interface Listener{
        void onEdit(Task task);
    }

    public void setListener(Listener listener){
        this.listener = listener;
    }

    private void onOK() {
        if (textField1.getText().length() == 0) {
            return;
        }

        Date time = spinnerDateModel.getDate();
        Date date = jxDatePicker.getDate();
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(time);
        GregorianCalendar gregorianCalendar1 = new GregorianCalendar();
        gregorianCalendar1.setTime(date);
        gregorianCalendar.set(Calendar.MONTH, gregorianCalendar1.get(Calendar.MONTH));
        gregorianCalendar.set(Calendar.YEAR, gregorianCalendar1.get(Calendar.YEAR));
        gregorianCalendar.set(Calendar.DAY_OF_MONTH, gregorianCalendar1.get(Calendar.DAY_OF_MONTH));

        if (task == null) {
            task = new Task(textField1.getText(), textField2.getText(), gregorianCalendar.getTime(),
                    Arrays.asList(formattedTextField2.getText().split(",")), false);
        } else {
            task.setName(textField1.getText());
            task.setDescription(textField2.getText());
            task.setDateTime(gregorianCalendar.getTime());
            task.setContacts(Arrays.asList(formattedTextField2.getText().split(",")));
        }

        listener.onEdit(task);
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
}
