package taskmanager;

import javax.swing.*;
import java.awt.event.*;

public class ErrorDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private Listener listener;
    JTextArea ERRORTextArea;

    public ErrorDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onReconnect();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    public interface Listener{
        void reconnect();
    }

    public void setListener(Listener listener){
        this.listener = listener;
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onReconnect(){
        dispose();
        listener.reconnect();
    }

    private void onCancel() {
        dispose();
    }

    public static void main(String[] args) {
        ErrorDialog dialog = new ErrorDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
