package taskmanager;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class GeneralForm {

    public JPanel panel;
    private JButton button1;
    private JTable table1;
    private JButton addButton;
    private JButton editButton;

    public GeneralForm() {
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showDialogEdit();
            }
        });
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showDialogAdd();
            }
        });
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showDialog();
            }
        });

        taskController = new TaskController();
        taskController.setOnChangeListener(new TaskController.OnChangeListener() {
            @Override
            public void onChanged() {
                save();
                reloadTable();
            }
        });
        try {
            FileInputStream reader = new FileInputStream("./events.xml");
            taskController.restore(reader);
            reloadTable();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    TaskController taskController;

    void reloadTable(){
        DefaultTableModel tableModel;
        tableModel = new DefaultTableModel();
        tableModel.addColumn("Name");
        tableModel.addColumn("Description");
        tableModel.addColumn("Date time");
        tableModel.addColumn("Contacts");
        for(int i = 0; i < taskController.getTaskList().size(); i++){
            Task taskt = taskController.getTaskList().get(i);
            tableModel.addRow(new String[]{taskt.getName(), taskt.getDescription(),
                    taskt.getDateTime().toString(), taskt.getContacts().toString()});
        }
        table1.setModel(tableModel);
    }

    void showDialogAdd(){
        AddDialog add = new AddDialog();
        add.pack();
        add.setListener(new AddDialog.Listener() {
            @Override
            public void onEdit(Task task) {
                taskController.create(task);
            }
        });
        add.setVisible(true);
    }

    void showDialog(){
        DeleteDialog delete = new DeleteDialog();
        delete.pack();
        delete.setDeleteListener(new DeleteDialog.DeleteListener() {
            @Override
            public void onDelete() {
                int index = table1.getSelectedRow();
                Task task = taskController.getTaskList().get(index);
                taskController.delete(task.getId());
            }
        });
        delete.setVisible(true);
    }

    void showDialogEdit(){
        AddDialog editDialog = new AddDialog();
        editDialog.setTask(taskController.getTaskList().get(table1.getSelectedRow()));
        editDialog.pack();
        editDialog.setListener(new AddDialog.Listener() {
            @Override
            public void onEdit(Task task) {
                Task taskToUpdate = taskController.getTaskList().get(table1.getSelectedRow());
                taskToUpdate.setName(task.getName());
                taskToUpdate.setDescription(task.getDescription());
                taskToUpdate.setDateTime(task.getDateTime());
                taskToUpdate.setContacts(task.getContacts()); //for "[]" убрать ковычки
                taskController.update(taskToUpdate);
            }
        });
        editDialog.setVisible(true);
        // save task controller
        save();
    }

    void runCheck(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (;;) {
                        Thread.sleep(5000);
                        for (Task task : taskController.taskList) {
                            Date date = new Date();
                            if (task.getDateTime().before(date) && task.isOccured() == false) {
                                task.setOccured(true);
                                save();
                                IsOccured isOccured = new IsOccured();
                                isOccured.setTask(task);
                                isOccured.pack();
                                isOccured.setVisible(true);
                                reloadTable();
                            }
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    void save() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FileOutputStream writer = new FileOutputStream("./events.xml");
                    taskController.save(writer);
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        }).start();
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
