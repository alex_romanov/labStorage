package taskmanager;

import java.util.Date;
import java.util.List;

public class Task {
    private String id;
    private String name;
    private String description;
    private Date dateTime;
    private List contacts;
    private boolean isOccured;

    @Override
    public String toString() {
        return "Task:" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", discription='" + description + '\'' +
                ", dateTime=" + dateTime +
                ", contacts=" + contacts;
    }

    public Task(String name, String discription, Date dateTime, List contacts, boolean isOccured) {
        this.id = id;
        this.name = name;
        this.description = discription;
        this.dateTime = dateTime;
        this.contacts = contacts;
        this.isOccured = isOccured;
    }

    public boolean isOccured() {
        return isOccured;
    }

    public void setOccured(boolean occured) {
        isOccured = occured;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String discription) {
        this.description = discription;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public List getContacts() {
        return contacts;
    }

    public void setContacts(List contacts) {
        this.contacts = contacts;
    }
}
