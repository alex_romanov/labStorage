package taskmanager;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TaskController {
    List<Task> taskList = new ArrayList<>();
    OnChangeListener onChangeListener;

    public void setOnChangeListener(OnChangeListener onChangeListener){
        this.onChangeListener = onChangeListener;
    }

    public interface OnChangeListener{
        void onChanged();
    }

    void update(Task update){
        for(int i = 0; i < taskList.size(); i++){
            if(taskList.get(i).getId().equals(update.getId())){
                taskList.set(i, update);
                onChangeListener.onChanged();
            }
        }
    }

    void create(Task task){
        taskList.add(task);
        task.setId(UUID.randomUUID().toString());
        onChangeListener.onChanged();
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
        onChangeListener.onChanged();
    }

    void delete(String id){
        for(int i = 0; i < taskList.size(); i++){
            if(taskList.get(i).getId().equals(id)){
                taskList.remove(i);
            }
        }
        onChangeListener.onChanged();
    }

    void showAllTasks(){
        for(Task task : taskList){
            System.out.println(task);
        }
    }

    void save(OutputStream out) throws IOException {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.newDocument();
            Element root = document.createElement("root");
            document.appendChild(root);
            for (Task task : taskList) {
                Element taskElement = document.createElement("task");
                Element IDElement = document.createElement("id");
                Element nameElement = document.createElement("name");
                Element descriptionElement = document.createElement("description");
                Element dateElement = document.createElement("date");
                Element contactsElement = document.createElement("contacts");
                Element isOccuredElement = document.createElement("isOccured");
                nameElement.appendChild(document.createTextNode(task.getName()));
                IDElement.appendChild(document.createTextNode(task.getId()));
                descriptionElement.appendChild(document.createTextNode(task.getDescription()));

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
                Date date = task.getDateTime();
                String dateString = formatter.format(date);
                dateElement.appendChild(document.createTextNode(dateString));

                StringBuilder stringBuilder = new StringBuilder();
                for(int i = 0; i < task.getContacts().size(); i++){
                    stringBuilder.append(task.getContacts().get(i));
                    if(i != task.getContacts().size() - 1){
                        stringBuilder.append(",");
                    }
                }
                contactsElement.appendChild(document.createTextNode(stringBuilder.toString()));
                isOccuredElement.appendChild(document.createTextNode(Boolean.toString(task.isOccured())));

                taskElement.appendChild(nameElement);
                taskElement.appendChild(IDElement);
                taskElement.appendChild(descriptionElement);
                taskElement.appendChild(dateElement);
                taskElement.appendChild(contactsElement);
                taskElement.appendChild(isOccuredElement);
                root.appendChild(taskElement);
            }
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(out);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.transform(source, result);
        } catch (ParserConfigurationException e) {
            throw new IOException(e);
        } catch (TransformerException e) {
            throw new IOException(e);
        }
    }

    void restore(InputStream is) throws IOException {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(is);

            Node root = document.getElementsByTagName("root").item(0);
            NodeList tasks = root.getChildNodes();

            for (int i = 0; i < tasks.getLength(); i++){
                Node taskNode = tasks.item(i);
                NodeList children = taskNode.getChildNodes();
                Task task = new Task("", "", new Date(), new ArrayList(), false);
                for (int j = 0; j < children.getLength(); j++) {
                    Node node = children.item(j);
                    if (node.getNodeName().equals("name")) {
                        task.setName(node.getTextContent());
                    }
                    if (node.getNodeName().equals("id")) {
                        task.setId(node.getTextContent());
                    }
                    if (node.getNodeName().equals("description")) {
                        task.setDescription((node.getTextContent()));
                    }
                    if (node.getNodeName().equals("date")) {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
                        String dateInString = node.getTextContent();
                        Date date = formatter.parse(dateInString);
                        task.setDateTime(date);
                    }
                    if (node.getNodeName().equals("contacts")){
                        task.setContacts(Arrays.asList(node.getTextContent().split(",")));
                    }
                    if (node.getNodeName().equals("isOccured")){
                        task.setOccured(Boolean.parseBoolean(node.getTextContent()));
                    }
                }
                taskList.add(task);
            }

            // root->task
        } catch (ParserConfigurationException e) {
            throw new IOException(e);
        } catch (SAXException e) {
            throw new IOException(e);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
