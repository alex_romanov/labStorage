package taskmanager;

import javax.swing.*;

public class Main {
    public static void main(String args[]){
        JFrame frame = new JFrame("GeneralForm");
        GeneralForm generalForm = new GeneralForm();
        generalForm.runCheck();
        frame.setContentPane(generalForm.panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
