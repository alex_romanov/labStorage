package taskmanager;

import com.sun.org.apache.regexp.internal.RE;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class GeneralForm implements ErrorDialog.Listener {
    public JPanel panel;
    private JButton button1;
    private JTable table1;
    private JButton addButton;
    private JButton editButton;
    private JButton refreshButton;

    public GeneralForm() {
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                taskController.showAllTasks();
            }
        });
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showDialogAdd();
            }
        });
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showDialog();
            }
        });
        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reconnect();
            }
        });
        reconnect();
    }

    TaskController taskController;

    void reloadTable() {
        DefaultTableModel tableModel;
        tableModel = new DefaultTableModel();
        tableModel.addColumn("Name");
        tableModel.addColumn("Description");
        tableModel.addColumn("Date time");
        tableModel.addColumn("Contacts");
        for (int i = 0; i < taskController.getTaskList().size(); i++) {
            Task taskt = taskController.getTaskList().get(i);
            tableModel.addRow(new String[]{taskt.getName(), taskt.getDescription(),
                    taskt.getDateTime().toString(), taskt.getContacts().toString()});
        }
        table1.setModel(tableModel);
    }

    void showDialogAdd() {
        AddDialog add = new AddDialog();
        add.pack();
        add.setListener(new AddDialog.Listener() {
            @Override
            public void onEdit(Task task) {
                try {
                    taskController.create(task);
                } catch (Exception e) {
                    showDialogError("disconected");
                    e.printStackTrace();
                }
            }
        });
        add.setVisible(true);
    }

    void showDialog() {
        DeleteDialog delete = new DeleteDialog();
        delete.pack();
        delete.setDeleteListener(new DeleteDialog.DeleteListener() {
            @Override
            public void onDelete() {
                int index = table1.getSelectedRow();
                Task task = taskController.getTaskList().get(index);
                try {
                    taskController.delete(task.getId());
                }catch (Client.TaskNotFoundExeption exeption) {
                    showDialogError("task not found");
                }
                catch(Exception e) {
                    showDialogError("disconected");
                    e.printStackTrace();
                }
            }
        });
        delete.setVisible(true);
    }

    void showDialogError(String error) {
        ErrorDialog errorDialog = new ErrorDialog();
        errorDialog.ERRORTextArea.append(error);
        errorDialog.setListener(this);
        errorDialog.pack();
        errorDialog.setVisible(true);
    }

    void showDialogEdit() {
        AddDialog editDialog = new AddDialog();
        editDialog.setTask(taskController.getTaskList().get(table1.getSelectedRow()));
        editDialog.pack();
        editDialog.setListener(new AddDialog.Listener() {
            @Override
            public void onEdit(Task task) {
                Task taskToUpdate = taskController.getTaskList().get(table1.getSelectedRow());
                taskToUpdate.setName(task.getName());
                taskToUpdate.setDescription(task.getDescription());
                taskToUpdate.setDateTime(task.getDateTime());
                taskToUpdate.setContacts(task.getContacts());
                try {
                    taskController.update(taskToUpdate);
                } catch (Client.TaskNotFoundExeption exeption) {
                    showDialogError("task not found");
                } catch (IOException e) {
                    showDialogError("disconected");
                    e.printStackTrace();
                }
            }
        });
        editDialog.setVisible(true);
        // save task controller
    }

    void delayTask(Task task) {
        Date date = task.getDateTime();
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        gregorianCalendar.add(Calendar.MINUTE, 15);
        task.setDateTime(gregorianCalendar.getTime());
        task.setOccured(false);
        try {
            taskController.update(task);
        } catch (Client.TaskNotFoundExeption exeption) {
            showDialogError("task not found");
        } catch (IOException e) {
            showDialogError("disconected");
            e.printStackTrace();
        }
    }

    void runCheck() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (; ; ) {
                        Thread.sleep(5000);
                        for (final Task task : taskController.taskList) {
                            Date date = new Date();
                            if (task.getDateTime().before(date) && task.isOccured() == false) {
                                task.setOccured(true);
                                IsOccured isOccured = new IsOccured();
                                isOccured.setTask(task);
                                isOccured.pack();
                                isOccured.setListener(new IsOccured.Listener() {
                                    @Override
                                    public void onDelay() {
                                        SwingUtilities.invokeLater(new Runnable() {
                                            @Override
                                            public void run() {
                                                delayTask(task);
                                            }
                                        });
                                    }

                                    @Override
                                    public void onDelete() {
                                        SwingUtilities.invokeLater(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    taskController.delete(task.getId());
                                                } catch (Exception e) {
                                                    showDialogError("disconected");
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                    }
                                });
                                isOccured.setVisible(true);
                                reloadTable();
                            }
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void reconnect() {
        try {
            taskController = new TaskController();
            taskController.restore();
            reloadTable();
            taskController.setOnChangeListener(new TaskController.OnChangeListener() {
                @Override
                public void onChanged() {
                    reloadTable();
                }
            });
        } catch (Exception e) {
            showDialogError("disconected");
            e.printStackTrace();
        }
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
