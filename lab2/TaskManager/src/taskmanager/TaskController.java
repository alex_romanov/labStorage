package taskmanager;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TaskController {
    Client client;
    List<Task> taskList = new ArrayList<>();
    OnChangeListener onChangeListener;

    public TaskController() throws Exception{
        client = new Client();
    }

    public void setOnChangeListener(OnChangeListener onChangeListener){
        this.onChangeListener = onChangeListener;
    }

    public interface OnChangeListener{
        void onChanged();
    }

    void update(Task update) throws IOException, Client.TaskNotFoundExeption {
        client.editTask(update);
        for(int i = 0; i < taskList.size(); i++){
            if(taskList.get(i).getId().equals(update.getId())){
                taskList.set(i, update);
                onChangeListener.onChanged();
            }
        }
    }

    void create(Task task) throws Exception{
        client.addTask(task);
        taskList.add(task);
        onChangeListener.onChanged();
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    void delete(String id) throws Exception {
        client.deleteTask(id);
        for(int i = 0; i < taskList.size(); i++){
            if(taskList.get(i).getId().equals(id)){
                taskList.remove(i);
            }
        }
        onChangeListener.onChanged();
    }

    void showAllTasks(){
        for(Task task : taskList){
            System.out.println(task);
        }
    }

    void restore() throws Exception {
        try {
            StringBufferInputStream stringBufferInputStream = new StringBufferInputStream(client.getAll());
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(stringBufferInputStream);

            Node root = document.getElementsByTagName("root").item(0);
            NodeList tasks = root.getChildNodes();

            for (int i = 0; i < tasks.getLength(); i++){
                Node taskNode = tasks.item(i);
                NodeList children = taskNode.getChildNodes();
                Task task = new Task("", "", new Date(), new ArrayList(), false);
                for (int j = 0; j < children.getLength(); j++) {
                    Node node = children.item(j);
                    if (node.getNodeName().equals("name")) {
                        task.setName(node.getTextContent());
                    }
                    if (node.getNodeName().equals("id")) {
                        task.setId(node.getTextContent());
                    }
                    if (node.getNodeName().equals("description")) {
                        task.setDescription((node.getTextContent()));
                    }
                    if (node.getNodeName().equals("date")) {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z");
                        String dateInString = node.getTextContent();
                        Date date = formatter.parse(dateInString);
                        task.setDateTime(date);
                    }
                    if (node.getNodeName().equals("contacts")){
                        task.setContacts(Arrays.asList(node.getTextContent().split(",")));
                    }
                    if (node.getNodeName().equals("isOccured")){
                        task.setOccured(Boolean.parseBoolean(node.getTextContent()));
                    }
                }
                taskList.add(task);
            }
            // root->task
        } catch (ParserConfigurationException e) {
            throw new IOException(e);
        } catch (SAXException e) {
            throw new IOException(e);
        } catch (ParseException e) {
            throw new IOException(e);
        }
    }
}
