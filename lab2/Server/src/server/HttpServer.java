package server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class HttpServer {
    static TaskController taskController;

    public static void main(String[] args) throws Throwable {
        ServerSocket ss = new ServerSocket(9485);
        taskController = new TaskController();
        while (true) {
            Socket s = ss.accept();
            System.err.println("Client accepted");
            new Thread(new SocketProcessor(s)).start();
        }
    }

    static void runCheck(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (;;) {
                        Thread.sleep(5000);
                        for (Task task : taskController.taskList) {
                            Date date = new Date();
                            if (task.getDateTime().before(date) && task.isOccured() == false) {
                                task.setOccured(true);
                                try {
                                    FileOutputStream writer = null;
                                    writer = new FileOutputStream("./events.xml");
                                    taskController.save(writer);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private static class SocketProcessor implements Runnable {

        private Socket s;
        private BufferedReader is;
        private BufferedWriter os;

        private SocketProcessor(Socket s) throws Throwable {
            this.s = s;
            this.is = new BufferedReader(new InputStreamReader(s.getInputStream()));
            this.os = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
            taskController = new TaskController();
            try {
                FileInputStream reader = new FileInputStream("./events.xml");
                taskController.restore(reader);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void run() {
            try {
                //writeResponse();
                String line = null;
                do {
                    line = is.readLine();
                    String out = "";
                    // TODO: /ADD
                    try {
                        if (line.equals("GETALL")) {
                            out = readFileToString(new File("./events.xml"));
                        } else if (line.equals("BYE")) {
                            line = null;
                        } else if (line.startsWith("ADD")) {
                            line = line.replaceAll("ADD ", "");
                            String[] fields = line.split(",");
                            out = addTask(fields);
                        } else if (line.startsWith("DELETE")) {
                            line = line.replaceAll("DELETE ", "");
                            String[] fields = line.split(",");
                            if(taskController.check(fields[0])){
                                deleteTask(fields[0]);
                            }else{
                                out = "Task not found";
                            }
                        } else if (line.startsWith("EDIT")) {
                            line = line.replaceAll("EDIT ", "");
                            String[] fields = line.split(",");
                            if(taskController.check(fields[0])){
                                editTask(fields);
                            }else{
                                out = "Task not found";
                            }
                        }
                    } catch (Exception e) {
                        out = "ERROR";
                    }
                    os.write(out, 0, out.length());
                    os.newLine();
                    os.flush();
                } while (line != null);
            } catch (Throwable t) {
                t.printStackTrace();
            } finally {
                try {
                    s.close();
                } catch (Throwable t) {

                }
            }
            System.err.println("Client processing finished");
        }

        public String addTask(String[] fields) throws Exception{
            Task task = new Task(null, null, null, null, false);
            task.setName(fields[0]);
            task.setDescription(fields[1]);
            String dateFormat = "yyyy.MM.dd HH:mm:ss z";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
            task.setDateTime(simpleDateFormat.parse(fields[2]));
            task.setContacts(Arrays.asList(fields[3].split("/")));
            taskController.create(task);
            saveTask();
            return task.getId();
        }

        public void deleteTask(String id) throws Exception{
            taskController.delete(id);
            saveTask();
        }

        public String editTask(String[] fields) throws Exception{
            Task task = new Task(null, null, null, null, false);
            task.setId(fields[0]);
            task.setName(fields[1]);
            task.setDescription(fields[2]);
            String dateFormat = "yyyy.MM.dd HH:mm:ss z";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
            task.setDateTime(simpleDateFormat.parse(fields[3]));
            task.setContacts(Arrays.asList(fields[4].split("/")));
            taskController.update(task);
            saveTask();
            return task.getId();
        }

        public void saveTask() throws Exception {
            FileOutputStream writer = null;
            writer = new FileOutputStream("./events.xml");
            taskController.save(writer);

        }

        public static String readFileToString(File file) throws IOException {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String string = null;
            StringBuilder stringBuilder = new StringBuilder();
            try {
                while ((string = reader.readLine()) != null) {
                    stringBuilder.append(string);
                }
            } finally {
                reader.close();
            }
            return stringBuilder.toString();
        }
    }
}