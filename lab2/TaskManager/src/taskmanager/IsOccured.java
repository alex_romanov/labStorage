package taskmanager;

import javax.swing.*;
import java.awt.event.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class IsOccured extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JTextArea textArea1;
    private JButton DELETEButton;
    private JButton DELAYTO15MINButton;
    private Task task;
    Listener listener;

    public IsOccured() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        DELETEButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onDelete();
            }
        });

        DELAYTO15MINButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onDelay15();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    void setTask(Task task){
        this.task = task;
        textArea1.append("Name: ");
        textArea1.append(task.getName());
        textArea1.append("\n");
        textArea1.append("Description: ");
        textArea1.append(task.getDescription());
        textArea1.append("\n");
        textArea1.append("IS OCCURED");
    }

    public interface Listener{
        void onDelay();
        void onDelete();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    private void onDelay15(){
        listener.onDelay();
        dispose();
    }

    private void onOK(){
        dispose();
    }

    private void onDelete(){
        listener.onDelete();
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
}
