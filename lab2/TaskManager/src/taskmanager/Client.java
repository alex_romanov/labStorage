package taskmanager;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.concurrent.ExecutionException;

public class Client {
    BufferedWriter bufferedWriter;
    BufferedReader bufferedReader;

    public Client() throws Exception {
        int serverPort = 9485;
        String address = "127.0.0.1";

        InetAddress ipAddress = InetAddress.getByName(address);
        Socket socket = new Socket(ipAddress, serverPort);

        InputStream sin = socket.getInputStream();
        OutputStream sout = socket.getOutputStream();

        bufferedReader = new BufferedReader(new InputStreamReader(sin));
        bufferedWriter = new BufferedWriter(new OutputStreamWriter(sout));
    }

    public String addTask(Task task) throws Exception{
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ADD ");
        stringBuilder.append(task.getName());
        stringBuilder.append(",");
        stringBuilder.append(task.getDescription());
        stringBuilder.append(",");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z");
        String formateDate = simpleDateFormat.format(task.getDateTime());
        stringBuilder.append(formateDate);
        stringBuilder.append(",");
        for(int i = 0; i < task.getContacts().size(); i++){
            stringBuilder.append(task.getContacts().get(i));
            stringBuilder.append("/");
        }
        bufferedWriter.write(stringBuilder.toString(), 0, stringBuilder.length());
        bufferedWriter.newLine();
        bufferedWriter.flush();
        task.setId(bufferedReader.readLine());
        return task.getId();
    }

    public void deleteTask(String id) throws Exception {
        String command;
        command = "DELETE " + id;
        bufferedWriter.write(command, 0, command.length());
        bufferedWriter.newLine();
        bufferedWriter.flush();
        if(bufferedReader.readLine().equals("Task not found")){
            throw new TaskNotFoundExeption();
        }
    }
    public void editTask(Task task) throws IOException, TaskNotFoundExeption {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("EDIT ");
        stringBuilder.append(task.getId());
        stringBuilder.append(",");
        stringBuilder.append(task.getName());
        stringBuilder.append(",");
        stringBuilder.append(task.getDescription());
        stringBuilder.append(",");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z");
        String formateDate = simpleDateFormat.format(task.getDateTime());
        stringBuilder.append(formateDate);
        stringBuilder.append(",");
        for(int i = 0; i < task.getContacts().size(); i++){
            stringBuilder.append(task.getContacts().get(i));
            stringBuilder.append("/");
        }
        bufferedWriter.write(stringBuilder.toString(), 0, stringBuilder.length());
        bufferedWriter.newLine();
        bufferedWriter.flush();
        if(bufferedReader.readLine().equals("Task not found")){
            throw new TaskNotFoundExeption();
        }
    }

    static class TaskNotFoundExeption extends Exception{

    }

    public String getAll() throws Exception{
        bufferedWriter.write("GETALL", 0, 6);
        bufferedWriter.newLine();
        bufferedWriter.flush();
        String line = bufferedReader.readLine();
        return line;
    }
}
